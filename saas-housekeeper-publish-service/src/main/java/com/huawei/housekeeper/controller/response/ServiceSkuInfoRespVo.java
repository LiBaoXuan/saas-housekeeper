/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.controller.response;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author jWX1116205
 * @since 2022-01-17
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("根据skuId查询SKU响应vo")
public class ServiceSkuInfoRespVo {
    @ApiModelProperty("SkuID")
    private Long skuId;

    @ApiModelProperty("可选服务")
    private List<OptionProperty> selections;

    @ApiModelProperty("价格")
    private BigDecimal price;

    @ApiModelProperty("服务Id")
    private Long servcieId;

    @ApiModelProperty(value = "服务名称")
    private String serviceName;

    @ApiModelProperty(value = "服务描述")
    private String serviceDesc;

    @ApiModelProperty("图片地址")
    private String imgSrc;

    @Data
    public static class OptionProperty {
        @ApiModelProperty("选项Id")
        private Long optionId;

        @ApiModelProperty("选项服务ID")
        private Long selectionId;

        @ApiModelProperty("选项名称")
        private String optionName;

        @ApiModelProperty("规格名称")
        private String specName;
    }
}