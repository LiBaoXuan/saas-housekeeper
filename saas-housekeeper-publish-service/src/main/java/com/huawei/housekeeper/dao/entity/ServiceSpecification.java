/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.dao.entity;

import com.huawei.housekeeper.entity.BaseEntity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * 规格表
 *
 * @author jwx1116205
 * @since 2022-03-03
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@TableName("t_service_specification")
public class ServiceSpecification extends BaseEntity {

    /**
     * 服务ID
     */
    @TableField("service_id")
    private Long serviceId;

    /**
     * 规格名称
     */
    @TableField("name")
    private String name;

    /**
     * 乐观锁
     */
    @TableField("revision")
    private String revision;

    /**
     * 删除标志
     */
    @TableField("delete_flag")
    private String deleteFlag;

}