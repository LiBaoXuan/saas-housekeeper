// 服务域名
const DOMAIN = 'http://saas-housekeeper.cloudbu.cloud-onlinelab.cn';

module.exports = {
    publicPath: './',
    outputDir: 'dist',
    assetsDir: 'static',
    filenameHashing: true,
    lintOnSave: false,
    devServer: {
        hot: true,
        port: 8083,
        compress: true,
        allowedHosts: 'all',
        proxy: {
            '/api-admin': {
                target: "http://localhost:8500",
                pathRewrite: { '^/api-admin': 'http://localhost:8500' },
            },
        },
    },
    configureWebpack: {},
};
