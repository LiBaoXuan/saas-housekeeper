DROP TABLE IF EXISTS `t_service_image`;

-- housekeeperdb.t_service_image definition

CREATE TABLE `t_service_image` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `image_id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '图片id',
  `service_id` varchar(100) DEFAULT NULL COMMENT '服务id',
  `image_name` varchar(200) NOT NULL COMMENT '图片名称',
  `type` varchar(100) NOT NULL COMMENT '图片类型',
  `content` mediumblob NOT NULL COMMENT '图片二进制',
  `created_by` varchar(100) DEFAULT NULL COMMENT '创建人',
  `created_time` datetime DEFAULT NULL COMMENT '创建时间',
  `updated_by` varchar(100) DEFAULT NULL COMMENT '更新人',
  `updated_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;