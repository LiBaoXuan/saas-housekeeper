ALTER TABLE t_task
    CHANGE customer_id customer_id VARCHAR(36) NULL COMMENT '顾客id;用户中心上下文',
    CHANGE order_id order_id       BIGINT      NULL COMMENT '订单id;订单中心上下文';