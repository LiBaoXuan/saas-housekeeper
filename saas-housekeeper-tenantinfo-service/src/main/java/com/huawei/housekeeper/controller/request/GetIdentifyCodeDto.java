package com.huawei.housekeeper.controller.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

/**
 * 发送邮件
 *
 * @since 2022-10-20
 */
@Data
@ApiModel("发送邮件")
public class GetIdentifyCodeDto {

    @ApiModelProperty(value = "邮箱地址", required = true)
    @NotBlank(message = "邮箱地址必填")
    @Pattern(regexp = "\\w[-\\w.+]*@([A-Za-z0-9][-A-Za-z0-9]+\\.)+[A-Za-z]{2,14}", message = "您输入的邮箱格式不正确，请重新输入！")
    private String email;
}