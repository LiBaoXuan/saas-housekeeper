/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.model;

import lombok.Data;

import java.io.Serializable;

/**
 * 超级管理员身份
 *
 * @author lWX1128557
 * @since 2022-03-03
 */
@Data
public class AdminContext implements Serializable {

    private static final long serialVersionUID = 1L;

    private String name;

    private String password;

    private String role;
}
