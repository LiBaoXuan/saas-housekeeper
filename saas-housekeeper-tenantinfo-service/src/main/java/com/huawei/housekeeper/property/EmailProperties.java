package com.huawei.housekeeper.property;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 功能描述
 *
 * @since 2022-10-13
 */
@Data
@Component
@Configuration
@ConfigurationProperties(prefix = "email-validation")
public class EmailProperties {
    @ApiModelProperty(value = "邮箱验证开关")
    private Boolean enable;

    @ApiModelProperty(value = "主题")
    private String subject;

    @ApiModelProperty(value = "网址")
    private String website;

    @ApiModelProperty(value = "正文")
    private String content;
}