/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.service;

/**
 * 功能描述接口
 *
 * @author zWX1196307
 * @since 2022-10-12
 */
public interface MailService {

    void sendSimpleMail(String mailTo, String cc, String subject, String content);

    void sendHtmlMail(String mailTo, String cc, String subject, String content);
}
