/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.saashousekeeper.dbpool;

import com.huawei.saashousekeeper.customedprocessor.PoolRefreshProcessor;

/**
 * jdbc连接池
 *
 * @author lWX1156935
 * @since 2022/4/22
 */
public interface JdbcPool extends PoolStrategy {
    /**
     * 是否需要刷新连接池属性，重建连接池, 平滑切换
     *
     * @param jdbcPool 新的池连接属性
     * @param processor 处理器
     * @return 判断结果
     */
    boolean refresh(JdbcPool jdbcPool, PoolRefreshProcessor processor);
}
