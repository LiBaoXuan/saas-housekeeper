package com.huawei.housekeeper.entity;

import com.huawei.saashousekeeper.context.TenantContext;
import lombok.Data;

@Data
public abstract class TenantMqMessage extends BaseMqMessage {
    protected String tenantId = TenantContext.getDomain();
}
