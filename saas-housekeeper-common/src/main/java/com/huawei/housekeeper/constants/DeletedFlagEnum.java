/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.constants;

import lombok.Getter;

/**
 * 功能描述 删除标志枚举类
 *
 * @author jWX1116205
 * @since 2022-01-05
 */
@Getter
public enum DeletedFlagEnum {

    SERVICE_FLAGE_UNDELETED("0", "未被删除标志"),
    SERVICE_FLAGE_DELETED("1", "服务删除标志");

    String code;

    String message;

    DeletedFlagEnum(String code, String message) {
        this.code = code;
        this.message = message;
    }

}