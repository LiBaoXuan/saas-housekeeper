/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.util;

import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.huawei.housekeeper.constants.BaseConstant;
import org.springframework.http.HttpHeaders;

/**
 * 请求头工具类
 *
 * @author y00464350
 * @since 2022-02-16
 */
public class HeaderUtil {

    public static void setHeaders(HttpHeaders headers) {
        headers.add(BaseConstant.Header.HEADER_ORIGIN, StringPool.ASTERISK);
        headers.add(BaseConstant.Header.HEADER_METHODS, BaseConstant.Header.METHODS_VALUES);
        headers.add(BaseConstant.Header.HEADER_AGE, "3600");
        headers.add(BaseConstant.Header.HEADER_HEADERS, StringPool.ASTERISK);
    }
}
