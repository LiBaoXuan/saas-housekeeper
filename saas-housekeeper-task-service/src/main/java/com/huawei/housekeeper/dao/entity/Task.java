/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.dao.entity;

import com.huawei.housekeeper.entity.BaseEntity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 任务表
 *
 * @author jwx1116205
 * @since 2022-03-02
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@TableName("t_task")
public class Task extends BaseEntity {
    /**
     * 顾客id;用户中心上下文
     */
    @TableField("customer_id")
    private String customerId;

    /**
     * 订单id;订单中心上下文
     */
    @TableField("order_id")
    private Long orderId;

    /**
     * 顾客名称
     */
    @TableField("customer_name")
    private String customerName;

    /**
     * 电话号码
     */
    @TableField("customer_phone")
    private String customerPhone;

    /**
     * 服务时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField("appointment_time")
    private Date appointmentTime;

    /**
     * 地址
     */
    @TableField("address")
    private String address;

    /**
     * 服务名称;订单中心推送
     */
    @TableField("service_name")
    private String serviceName;

    /**
     * 服务细节;订单中心推送
     */
    @TableField("service_detail")
    private String serviceDetail;

    /**
     * 购物数量
     */
    @TableField("amount")
    private Integer amount;

    /**
     * 佣金,可通过支付金额得出
     */
    @TableField("salary")
    private BigDecimal salary;

    /**
     * 雇佣名称
     */
    @TableField("employee_name")
    private String employeeName;

    /**
     * 雇佣id
     */
    @TableField("employee_id")
    private String employeeId;

    /**
     * 任务状态
     */
    @TableField("task_status")
    private String taskStatus;

    /**
     * 乐观锁
     */
    @TableField("version")
    private String version;

    /**
     * 删除标志
     */
    @TableField("delete_flag")
    private String deleteFlag;

    /**
     * 订单备注
     */
    @TableField("remark")
    private String remark;
}