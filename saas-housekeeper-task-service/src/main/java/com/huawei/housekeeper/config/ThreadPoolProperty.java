/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.config;

import lombok.Getter;
import lombok.Setter;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Configuration;

/**
 * 功能描述
 *
 * @since 2022-05-24
 */
@Getter
@Setter
@Configuration
@RefreshScope
@ConfigurationProperties(prefix = "threadpool")
public class ThreadPoolProperty {
    // 是否开启配置
    private boolean enable;

    // 核心线程
    private int corePoolSize;

    // 最大线程
    private int maxPoolSize;

    // 队列容量
    private int queueCapacity;

    // 线程最大空闲时间
    private int keepAliveSeconds;
}