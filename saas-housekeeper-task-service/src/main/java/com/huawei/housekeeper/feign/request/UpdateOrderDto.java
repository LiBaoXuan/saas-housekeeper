
package com.huawei.housekeeper.feign.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * 用户更新订单
 *
 * @author lWX1128557
 * @since 2022-04-08
 */
@Data
@ApiModel(value = "用户更新订单")
public class UpdateOrderDto {

    @NotBlank(message = "必填")
    @ApiModelProperty(value = "订单编号")
    private String orderNumber;

    @NotNull
    //
    @Min(value = 2)
    @Max(value = 4)
    @ApiModelProperty(value = "订单状态")
    private Integer status;
}
