/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.controller.request;

import com.huawei.housekeeper.request.PageRequest;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 用户分页查询对象
 *
 * @author l84165417
 * @since 2022/1/26 15:18
 */
@Data
@ApiModel("用户分页查询对象")
public class PageQueryAllUserDto extends PageRequest {
    @ApiModelProperty(value = "用户类型")
    private Integer accountType;
}