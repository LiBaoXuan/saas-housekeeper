
/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.filters;

import com.huawei.housekeeper.enums.ErrorCode;
import com.huawei.housekeeper.result.Result;
import com.huawei.housekeeper.utils.JsonUtil;
import com.huawei.housekeeper.utils.JwtTokenUtil;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 当未登录或者token失效访问接口时，自定义的返回结果
 */
@Component
public class RestAuthenticationEntryPoint implements AuthenticationEntryPoint {
    private final static Logger logger = LoggerFactory.getLogger(RestAuthenticationEntryPoint.class);

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Value("${jwt.tokenHead}")
    private String tokenHead;

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response,
        AuthenticationException authException) throws IOException, ServletException {
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json");
        response.setHeader("Access-Control-Expose-Headers", "Authorization, Metis-Role");
        response.setHeader("Access-Control-Allow-Methods", "POST,GET,OPTIONS,DELETE");
        response.setHeader("Access-Control-Allow-Origin", "*");
        Result<String> result = null;
        String authHeader = request.getHeader(HttpHeaders.AUTHORIZATION);
        if (StringUtils.isNotBlank(authHeader) && authHeader.startsWith(tokenHead)) {
            String token = authHeader.substring(tokenHead.length());
            try {
                if (jwtTokenUtil.isTokenExpired(token)) {
                    result = Result.createResult(ErrorCode.TOKEN_EXPIRED);
                }
            } catch (Exception e) {
                logger.error("Token非法，token:{}", authHeader);
                result = Result.createResult(ErrorCode.TOKEN_ERROR);
            }
        } else {
            logger.error("Token非法");
            result = Result.createResult(ErrorCode.TOKEN_ERROR);
        }
        response.getWriter().println(JsonUtil.objectToJson(result));
        response.setStatus(401); // Aspect无法处理Adapter中错误，Adapter先执行
        response.getWriter().flush();
        response.flushBuffer();
    }
}
