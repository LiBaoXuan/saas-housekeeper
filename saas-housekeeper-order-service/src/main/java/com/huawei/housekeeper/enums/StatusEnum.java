/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.enums;

import lombok.Getter;

/**
 * 订单状态
 *
 * @author lWX1128557
 * @since 2022-02-23
 */
@Getter
public enum StatusEnum {

    /**
     * 待接单
     */
    WAITING(1, "waiting"),

    /**
     * 已完成
     */
    COMPLETED(2, "completed"),

    /**
     * 取消
     */
    CANCEL(3, "cancel"),

    /**
     * 进行中
     */
    ONGOING(4, "ongoing");

    private Integer status;

    private String action;

    StatusEnum(Integer status, String action) {
        this.status = status;
        this.action = action;
    }
}
