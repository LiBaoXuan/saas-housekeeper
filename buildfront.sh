#!/bin/bash
rm -rf html
mkdir html
npm config set registry https://registry.npm.taobao.org

cd saas-housekeeper-web-customer
npm i  --legacy-peer-deps
npm cache clear --force
npm cache verify
rm -rf package-lock.json
npm run build
cd ..
mv saas-housekeeper-web-customer/dist html/customer

cd saas-housekeeper-web-super-admin
npm i  --legacy-peer-deps
npm cache clear --force
npm cache verify
rm -rf package-lock.json
npm run build
cd ..
mv saas-housekeeper-web-super-admin/dist html/super-admin

cd saas-housekeeper-web-tenant
npm i  --legacy-peer-deps
npm cache clear --force
npm cache verify
rm -rf package-lock.json
npm run build
cd ..
mv saas-housekeeper-web-tenant/dist html/tenant

cd saas-housekeeper-web-worker
npm i  --legacy-peer-deps
npm cache clear --force
npm cache verify
rm -rf package-lock.json
npm run build
cd ..
mv saas-housekeeper-web-worker/dist html/worker
